//set up ===
var express = require('express');
var app     = express(); //create our app with express
var mongoose = require('mongoose'); //mongoose for mongodb

//configuration
mongoose.connect('mongodb://127.0.0.1:27017/test');

app.configure(function() {
    app.use(express.static(__dirname + '/public')); //set static file locations to default to public
    app.use(express.logger('dev')); //log request to Console
    app.use(express.bodyParser()); //pull information from html in post
    app.use(express.methodOverride()); //simulate DELETE and PUT
});

var Todo = mongoose.model('Todo', {
    text: string
});

app.listen(3000);
console.log('App listening on port 8080');

//routes
//api
app.get('/api/todos', function(req, res) {
    //use mongoose to find all todos in the database
    Todo.find(function(err, todos) {
        //if there is an error send it
        if(err)
            res.send(err);
        res.json(todos); //return all todos in json format
    });
});

app.post('/api/todos', function(req, res) {
    //create a todos, information comes from ajax request
    Todo.create({
        text: req.body.text,
        done: false
    }, function(err, todo){
        if(err)
            res.send(err);

        //get and return all todos created
        Todo.find(function(err, todos) {
            if(err)
                res.send(err)
            res.json(todos);
        });
    });
});

app.delete('api/todos/:todo_id', function(req, res) {
    Todo.remove({
        _id : req.params.todo_id
    }, function(err, todo) {
        if (err)
            res.send(err);
        Todo.find(function(err, todos) {
            if(err)
                res.send(err)
            res.json(todos);
        });
    });
});